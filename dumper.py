#!/usr/bin/env python

'''
Python script to download videos from NRK based on web url
Require Python2, BeautifulSoup, Requests and libav-tools to be installed

Install them as follows:

$ sudo apt-get install libav-tools
$ sudo apt-get install python-pip
$ sudo pip install BeautifulSoup4
$ sudo pip install Requests

Set DOWNLOAD_PATH and USER_AGENT as needed
'''

from bs4 import BeautifulSoup as bs
import requests, json, urllib2, urlparse, os, subprocess, re
from time import sleep
import sys

USER_AGENT = 'Python/2.7, Python CLI Stream App/0.1'
USER_HOME = '/media/archive' # Change this..
DOWNLOAD_PATH = USER_HOME + '/NRKSuperVideos'


class NRKSuperDump(object):
    def __init__(self):
        return None
        
    def download_clip(self, url, program_type, season):
        '''Takes an url and download the actual clip
        '''
        FNULL = open(os.devnull, 'w')
        response = requests.get(url)
        webpage = bs(response.text, "html.parser")
        link = webpage.find("div", { "id" : "playerelement" })
        m_url = link.get('data-hls-media')
        
        sleep(1) # be nice

        # Get best video url from master.m3u8
        req = urllib2.Request(m_url, headers={'User-Agent': USER_AGENT})
        response = urllib2.urlopen(req)
        lines = response.readlines()
        index_urls = []
        r = re.compile(r'_av.m3u8')
        for i in range(len(lines)):
            if r.search(lines[i]):
                index_urls.append(lines[i])
        d_url = index_urls[-1]
        d_url = d_url.replace('\n', '').replace('\r', '')
        #print d_url
                
        if program_type == "Series":
            # For series and not programs, fix the naming to suit Plex, ie: "title - s01e01" from "title - 1:1"
            # Extract numbers from the filename into a list. Not correct as season isn't captured yet.
            serie_num_list = re.findall(r'\d+', webpage.title.text)
            # Remove last part of filename up until first space, before the numbering
            title = webpage.title.text
            title = title.replace('NRK Super TV - ','')
            # Remove the last part and the beginning NRK Super TV text
            serie_name_split = title.rsplit('-', 1)
            title = title.replace(serie_name_split[1], ' ')
            # Add the numbers in correct format, ie s1e1
            title = title + 's' + season + 'e' + serie_num_list[0]
            #print title
            # Add the filename extension, ie .mp4
            title = title + '.mp4'
        elif program_type == "Program":
            # For single programs, just remove the leading text
            title = webpage.title.text + ' - s1e1.mp4'
            title = title.replace('NRK Super TV - ','').replace('NRK TV - ','')
        
        # Check if clip already exists on drive
        if os.path.isfile(title):
            print title + ' has already been downloaded..skipping.'
            return
        else:           
            # Download clip
            print 'downloading: ' +  title
            subprocess.call(['avconv', '-i', d_url, '-c', 'copy', title], stdout=FNULL, stderr=subprocess.STDOUT)

    def get_json(self, webpage, domain):
        '''Get the json file based on the input webpage
        '''
        ddata = {}
        key = 1
        for link in webpage.findAll("div", { "class" : "season-episodes loading" }):
            json_url = 'http://' + domain + link.get('data-url')
            req = urllib2.Request(json_url, headers={'User-Agent': USER_AGENT})
            response = urllib2.urlopen(req)
            data = json.load(response)
            for k in data['data']:
                ddata[k['fullTitle']] = (k['url'], k['title'], k['availabilityText'], k['seasonNumber'], key)
                key = key + 1
            sleep(2)
        return ddata
        
    def clear_screen(self):
        '''Clears the screen
        '''
        os.system('cls' if os.name=='nt' else 'clear')

    def list_clips_available(self, clips):
        '''Lists all available clips and presents them nicely
        '''
        key = 1
        for k, v in sorted(clips.iteritems()):
            j = str(key)
            print '[' + str.rjust(j, 2) + '] ' + 'Sesong: ' + v[3] + ' - ' + k + ' - ' + v[2]
            key += 1    

    def create_folder(self, folder):
        '''Use the name of the series and create a download folder
        '''
        try:
            os.chdir(os.path.join(DOWNLOAD_PATH, folder))
        except OSError:
            try:
                os.chdir(os.path.join(DOWNLOAD_PATH))
            except OSError:
                print 'Could not open main directory: ' + DOWNLOAD_PATH + ', creating it'
                os.mkdir(os.path.join(DOWNLOAD_PATH), 0755)
                os.chdir(os.path.join(DOWNLOAD_PATH))
            print 'Could not open output directory: ' + folder + ', creating it'
            dir_temp = folder.replace('NRK Super TV - ','')
            dir_temp = folder.replace('NRK TV - ','')
            os.mkdir(os.path.join(DOWNLOAD_PATH, dir_temp), 0755)
            os.chdir(os.path.join(DOWNLOAD_PATH, dir_temp))
        if not os.path.isfile('description.txt'):
            # Get image and description for series/program
            txt = webpage.findAll(attrs={"name":"description"})
            series_desc = txt[0]['content'].encode('utf-8')
            f = open('description.txt', 'w+')
            f.write(series_desc)
            f.close()
        if not os.path.isfile('poster.jpg'):
            img = webpage.findAll(attrs={"name":"artikkel_frimerke"})
            series_img = img[0]['content'].encode('utf-8')
            FNULL = open(os.devnull, 'w')
            subprocess.call(['wget', series_img, '-O', 'poster.jpg'], stdout=FNULL, stderr=subprocess.STDOUT)
            print 'Downloaded description and image for the series too.'
        
if __name__ == '__main__':
    # Construct an instance of the class and 
    superdump = NRKSuperDump()
    superdump.clear_screen()
    try:
        url = sys.argv[1]
    except IndexError:
        url = raw_input("URL: ")
    headers = {'User-Agent': USER_AGENT}
    response = requests.get(url, headers=headers)
    data = response.text
    webpage = bs(data, "html.parser")
    # Get the program_type for the download function
    meta_tag = webpage.findAll(attrs={"name":"urltype"})
    program_type = meta_tag[0]['content'].encode('utf-8')
    domain = urlparse.urlsplit(url)[1].split(':')[0]
    series = ''
    try:
        print 'Getting data...\n'
        clips = superdump.get_json(webpage, domain)
        series = clips.values()[0][1]
        superdump.list_clips_available(clips)
        choice = ''
        loop = 1

        # the main loop to present the menu
        while loop == 1:
            print '\n   X: Choose a clip number [ X ] to download'
            print '   A: Download all clips available'
            print '   Q: Quit\n'
            choice = raw_input('Choose an option: ')
            if choice == 'A' or choice == 'a':
                superdump.create_folder(series)
                for i in clips.values():
                    clip = 'http://' + domain + i[0]
                    season = i[3]
                    superdump.download_clip(clip, program_type, season)
                print '\nDownload complete!'
                loop = 0
            elif choice == 'Q' or choice == 'q':
                loop = 0
            else:
                try:
                    superdump.create_folder(series)
                    sorted_clips = sorted(clips.iteritems())
                    clip = 'http://' + domain + sorted_clips[int(choice)-1][1][0]
                    season = clips.values()[0][3]
                    superdump.download_clip(clip, program_type, season)
                    print '\nDownload complete!'
                    loop = 0
                except IndexError:
                    print 'Use a number in the list above or chose another option'
                    sleep(2)

    except AttributeError:
        choice = ''
        loop = 1
        jdata = ""
        clips = ""

        title = webpage.title.text
        title = title.replace('NRK Super TV - ','')
        # Get the program_type for the download function
        meta_tag = webpage.findAll(attrs={"name":"urltype"})
        program_type = meta_tag[0]['content'].encode('utf-8')
        print 'Not a series, ' + title + ' is just a single program.'

        # the main loop to present the menu
        while loop == 1:
            print ' '
            print 'Download the clip?'
            choice = raw_input('yes or no:')
            if choice == 'y' or choice == 'yes':
                superdump.create_folder(title)
                superdump.download_clip(url, program_type, season)
                print '\nDownload complete!'
                loop = 0
            elif choice == 'n' or choice == 'no':
                loop = 0
    except IndexError:
        print 'Could not find any show on this URL: ' + url
