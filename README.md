NRKSuperDump
============

Python script to download MP4 videos from NRK based on web url
Require Python2, BeautifulSoup, Requests and libav-tools to be installed

Install them as follows:

```bash
$ sudo apt-get install libav-tools
$ sudo apt-get install python-pip
$ sudo pip install BeautifulSoup4
$ sudo pip install Requests
```

HOW TO USE IT:
-------
Change USER_HOME to your storage location, ie an external mounted harddrive

```bash
USER_HOME = os.getenv('/media/archive')
```

Run the script as:

```bash
$ python dumper.py
```
You will then be requested to enter a full URL to a NRK Super TV series:

```bash
Enter URL to extract clips from (ex. http://tv.nrksuper.no/serie/bien-maja): 
```

Use for example:

+ http://tv.nrksuper.no/serie/bien-maja
+ http://tv.nrksuper.no/serie/alle-vi-barna-i-bakkebygrenda
+ http://tv.nrksuper.no/serie/blekksprut
+ http://tv.nrksuper.no/serie/georg-krymp
+ http://tv.nrksuper.no/serie/jul-i-skomakergata

![](http://gfx.nrk.no/8sRT_QPaVu33e0-D0PtokwI4jkvXS9yXoyKGrggZeuiw "Jul i skomakergata")

WISHLIST
----------------

+ Parameter for running in batch mode, with textfile containing lines of urls as parameter
+ Add .nfo creator to better support importing into xbmc or plex
